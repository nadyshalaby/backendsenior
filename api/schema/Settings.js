class Settings {
  static getCollectionName() {
    return "settings";
  }

  static async first() {
    const data = [];

    (
      await db
        .collection(Settings.getCollectionName())
        .get()
    ).forEach((s) => data.push(s.data()));

    return data.length && data.pop();
  }

  /**
   * Find by where clauses.
   *
   * @param {Array<Array>} conditions
   * @returns {object|boolean}
   */
  static async find(conditions) {
    const data = [];

    let query = db.collection(Settings.getCollectionName());

    for (const condition of conditions) {
      query = query.where(condition[0], condition[1], condition[2]);
    }

    (await query.get()).forEach((s) => data.push(s.data()));

    return data;
  }

  static query() {
    return db.collection(Settings.getCollectionName());
  }

  static async truncate() {
    return await db
      .collection(Settings.getCollectionName())
      .get()
      .then((res) => {
        res.forEach((element) => {
          element.ref.delete();
        });
      });
  }
}

module.exports = Settings;

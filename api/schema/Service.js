class Service {
  constructor(name, description, url) {
    this.setId(sails.helpers.randomCryptoString.with({ size: 32 }));
    this.setName(name);
    this.setDescription(description);
    this.setUrl(url);
    this.setEnabled(true);
    this.setCreatedAt(Date.now());
    this.setUpdatedAt(Date.now());
  }

  static getCollectionName() {
    return "services";
  }

  getId() {
    return this.id;
  }

  getName() {
    return this.name;
  }

  getDescription() {
    return this.description;
  }

  getUrl() {
    return this.url;
  }

  isEnabled() {
    return this.enabled;
  }

  getCreatedAt() {
    return this.createdAt;
  }

  getUpdatedAt() {
    return this.updatedAt;
  }

  setId(id) {
    this.id = id;
  }

  setName(name) {
    this.name = name;
  }

  setDescription(description) {
    this.description = description;
  }

  setUrl(url) {
    this.url = url;
  }

  setEnabled(enabled) {
    this.enabled = enabled;
  }

  setCreatedAt(createdAt) {
    this.createdAt = createdAt;
  }

  setUpdatedAt(updatedAt) {
    this.updatedAt = updatedAt;
  }

  async create() {
    return await db
      .collection(Service.getCollectionName())
      .add(this.toObject());
  }

  static async findById(id) {
    const data = [];

    (
      await db
        .collection(Service.getCollectionName())
        .where("id", "==", id)
        .get()
    ).forEach((s) => data.push(s.data()));

    return data.length && data.pop();
  }

  /**
   * Find by where clauses.
   *
   * @param {Array<Array>} conditions
   * @returns {object|boolean}
   */
  static async find(conditions) {
    const data = [];

    let query = db.collection(Service.getCollectionName());

    for (const condition of conditions) {
      query = query.where(condition[0], condition[1], condition[2]);
    }

    (await query.get()).forEach((s) => data.push(s.data()));

    return data;
  }

  static query() {
    return db.collection(Service.getCollectionName());
  }

  static async truncate() {
    return await db
      .collection(Service.getCollectionName())
      .get()
      .then((res) => {
        res.forEach((element) => {
          element.ref.delete();
        });
      });
  }

  toObject() {
    return {
      id: this.getId(),
      name: this.getName(),
      description: this.getDescription(),
      url: this.getUrl(),
      enabled: this.isEnabled(),
      createdAt: this.getCreatedAt(),
      updatedAt: this.getUpdatedAt(),
    };
  }
}

module.exports = Service;

class Appointment {
  constructor(serviceId, userId, timeSlot, date) {
    this.setId(sails.helpers.randomCryptoString.with({ size: 32 }));
    this.setServiceId(serviceId);
    this.setUserId(userId);
    this.setTimeSlot(timeSlot);
    this.setDate(date);
    this.setStatus('active');
    this.setCreatedAt(Date.now());
    this.setUpdatedAt(Date.now());
  }

  setServiceId(serviceId) {
    this.serviceId = serviceId;
  }

  getServiceId() {
    return this.serviceId;
  }

  setUserId(userId) {
    this.userId = userId;
  }
  getUserId() {
    return this.userId;
  }

  setStatus(status) {
    this.status = status;
  }

  getStatus() {
    return this.status;
  }
  
  setTimeSlot(timeSlot) {
    this.timeSlot = timeSlot;
  }

  getTimeSlot() {
    return this.timeSlot;
  }

  setDate(date) {
    this.date = date;
  }

  getDate() {
    return this.date;
  }

  getId() {
    return this.id;
  }

  getCreatedAt() {
    return this.createdAt;
  }

  getUpdatedAt() {
    return this.updatedAt;
  }

  setId(id) {
    this.id = id;
  }

  setCreatedAt(createdAt) {
    this.createdAt = createdAt;
  }

  setUpdatedAt(updatedAt) {
    this.updatedAt = updatedAt;
  }

  static getCollectionName() {
    return "appointments";
  }

  async create() {
    return await db
      .collection(Appointment.getCollectionName())
      .add(this.toObject());
  }

  static async truncate() {
    return await db
      .collection(Appointment.getCollectionName())
      .get()
      .then((res) => {
        res.forEach((element) => {
          element.ref.delete();
        });
      });
  }

  static async findById(id) {
    const data = [];

    (
      await db
        .collection(Appointment.getCollectionName())
        .where("id", "==", id)
        .get()
    ).forEach((s) => data.push(s.data()));

    return data.length && data.pop();
  }

  static query() {
    return db.collection(Appointment.getCollectionName());
  }

  /**
   * Find by where clause.
   *
   * @param {Array<Array>} conditions
   * @returns {object|boolean}
   */
  static async find(conditions) {
    const data = [];

    let query = db.collection(Appointment.getCollectionName());

    for (const condition of conditions) {
      query = query.where(condition[0], condition[1], condition[2]);
    }

    (await query.get()).forEach((s) => data.push(s.data()));

    return data;
  }

  toObject() {
    return {
      id: this.getId(),
      serviceId: this.getServiceId(),
      userId: this.getUserId(),
      timeSlot: this.getTimeSlot(),
      date: this.getDate(),
      status: this.getStatus(),
      createdAt: this.getCreatedAt(),
      updatedAt: this.getUpdatedAt(),
    };
  }
}

module.exports = Appointment;

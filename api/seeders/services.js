const Service = require("../schema/Service");

const services = [
  new Service(
    "Ward Facilities",
    `
   One of the distinguishing factors that marks hospitals unique, with respect to other health facilities; such as, clinics and care centers is that they provide both inpatient and outpatient care provisions.

Every hospital needs to have ward allotment for patients suffering from severe illness and injuries. This enables care givers to monitor health conditions continuously and thereby, accurate treatment procedures can be opted for.

In India, hospitals provide different types of wards for patients. Some of these are; general wards, private wards, semi private wards etc. Depending on choice and costs, patients can opt for any type of ward. Modern hospitals are sufficiently equipped to provide huge comfort to patients and are no less, or sometimes better than homes.
  `,
    "ward-facilities"
  ),
  new Service(
    "Nursing",
    `
  Nurses employed by hospitals function as connecting bridges between doctors and the patients they treat. Nurses look after the overall well being of patients. They ensure that all health related instructions given by doctors are implemented and followed by patients.

Nurses are employed in different departments within hospitals. Specialised hospitals hire trained nurses who are technically sound to perform advanced tasks relevant to diagnosis, treatment and care of sick ones.

Every hospital therefore has a separate nursing department which forms a core segment of hospitalisation.
  `,
    "nursing"
  ),
  new Service(
    "Out patient Department( OPD)",
    `
Every hospital needs to have an OPD (Outpatient Department) as one of the core services rendered. A set of skilled doctors along with technicians must be available, who can assist patients in preliminary diagnosis and related treatments.

Doctors prescribe medicines or diagnostic tests to evaluate patient’s health condition. They are also required to be available for follow up visits to monitor conditions and treatment procedures. Depending on the response, patients are either asked for admission or specialized services such as surgeries.
`,
    "out-patient-department-opd"
  ),
  new Service(
    "Intensive Care Unit(ICU)",
    `Almost every hospital in India provides ICU facilities for patients suffering from adverse health conditions. ICUs need to be equipped with modern technical apparatus and skilled professionals who can render best services. Depending on the type of hospital and services they focus on, there can be different types of ICUs. Some of them are, Neuro ICU, Coronary Care Unit, Neonatal ICU, Psychiatric ICU etc.`,
    "intensive-care-unit-icu"
  ),
  new Service(
    "Pharmacy and Diagnosis",
    `
  Most hospitals in India do maintain a pharmacy and diagnostic department of their own. This is a very important segment as easy and quick availability of medicines and other equipment required for treatment procedure is vital to render best healing services.

 However, many a times, hospitals do not guarantee the availability of all medicines that are being prescribed and therefore independent pharmacies are equally important. Further, 9 out of 10 hospitals maintain a separate laboratory and diagnostic section where related tests can be carried out. Again, similar to pharmacies, these diagnostic departments do not offer wide variety of test and analysis for patients.

These were the five most common services related to hospitalization. The more advanced a hospital is in rendering services, the more popular and desirable it becomes. Indian hospitals are aiming at becoming more advanced in providing best and upgraded amenities to patients that can ultimately enhance their treatment procedures. Thus, with upgraded hospital features, health care system in India will be at the top of providing best medical solutions to the entire population.

`,
    "pharmacy-and-diagnosis"
  ),
];

module.exports = async () => {
  await Service.truncate();
  services.forEach(async (service) => await service.create());
};

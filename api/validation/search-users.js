module.exports = joi
  .object({
    email: joi
      .string()
      .email()
      .error(new Error("Invalid Email.")),
    username: joi
      .string()
      .error(new Error("Invalid Username.")),
    page: joi
      .number()
      .min(0)
      .required()
      .error(new Error("Invalid page number")),
    limit: joi
      .number()
      .min(1)
      .required()
      .error(new Error("Invalid limit number")),
  })
  .unknown();

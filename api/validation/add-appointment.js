module.exports = joi
  .object({
    service: joi
      .string()
      .required()
      .error(new Error("Invalid service ID.")),
    time: joi
      .string()
      .regex(/^([0-9]{2})\:([0-9]{2})$/)
      .required()
      .error(new Error("Invalid time (format: HH:mm).")),
    date: joi
      .string()
      .regex(/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/)
      .required()
      .error(new Error("Invalid date (format: YYYY-MM-DD).")),
  })
  .unknown();

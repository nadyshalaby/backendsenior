const Appointment = require("../../schema/Appointment");
const moment = require("moment");
const Settings = require("../../schema/Settings");

module.exports = {
  friendlyName: "Add Appointment",

  description: "Add appointment.",

  inputs: {
    service: {
      type: "string",
    },
    date: {
      type: "string",
    },
    time: {
      type: "string",
    },
  },

  exits: {
    success: {
      responseType: "ok",
      description: "Appointment added successfully.",
    },
  },

  fn: async function (inputs, exits) {
    const settings = await Settings.first();

    if (!settings) {
      return this.res.badRequest("Settings not found");
    }

    const time = moment(
      `${inputs.date} ${inputs.time}`,
      "YYYY-MM-DD HH:mm"
    ).toDate();

    const timeSlot = await sails.helpers.timeSloter.with({
      time: inputs.time,
      startTime: moment(settings.startTime).format("HH:mm"),
      endTime: moment(settings.endTime).format("HH:mm"),
      nextSlot: settings.slotTime,
    });

    // Check if time slot is seleced.
    if (!timeSlot.selected) {
      return this.res.badRequest("There's no time slots match you time.");
    }

    // check if slot is available for this user.
    let appointment = {};

    try {
      if (
        await sails.helpers.canTakeAppointment.with({
          settings,
          slot: timeSlot.selected,
          time,
          user: this.req.user.id,
          service: inputs.service,
        })
      ) {
        
        appointment = new Appointment(
          inputs.service,
          this.req.user.id,
          timeSlot.selected,
          time
        );

        await appointment.create();
      }
    } catch (error) {
      return this.res.badRequest(error);
    }

    return exits.success(appointment.toObject());
  },
};

const moment = require("moment");
const Settings = require("../../schema/Settings");

module.exports = {
  friendlyName: "Nearest Appointments",

  description: "Nearest appointments.",

  inputs: {
  },

  exits: {
    success: {
      responseType: "ok",
      description: "Appointment added successfully.",
    },
  },

  fn: async function (inputs, exits) {
    const settings = await Settings.first();

    if (!settings) {
      return this.res.badRequest("Settings not found");
    }

    let appointments = [];
    
    try {
      appointments = await sails.helpers.findNearestAppointments.with({
        settings,
        user: this.req.user.id,
      });
    } catch (error) {
      return this.res.badRequest(error);
    }

    return exits.success(appointments);
  },
};

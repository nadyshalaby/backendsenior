/**
 * Index created user.
 *
 * @param {object} user
 * @returns {Promise<object>}
 */
const algoliaSearchIndex = async (search, criteria) => {
  return await algoliaIndex.search(search, criteria);
};

module.exports = {
  friendlyName: "Create user",
  description: "Create a new user.",

  inputs: {
    username: {
      type: "string",
    },

    email: {
      type: "string",
    },
    page: {
      type: "number",
      required: true,
    },
    limit: {
      type: "number",
      required: true,
    },
  },

  exits: {
    invalid: {
      responseType: "badRequest",
      description: "The provided inputs contains data are invalid.",
    },
    success: {
      responseType: "ok",
      description: "The provided inputs are valid.",
    },
  },

  fn: async function (inputs, exits) {
    try {
      //insert the user data to algolia index
      let filters = [];

      if (inputs.email) {
        filters.push('email:' + inputs.email);
      }

      if (inputs.username) {
        filters.push('username:' + inputs.username);
      }

      const results = await algoliaSearchIndex(
        '',
        {
          facetFilters: filters,
          page: inputs.page,
          hitsPerPage: inputs.limit,
        }
      );

      return this.res.successResponse({
        message: this.req.i18n.__("mission_success"),
        data: results,
      });
    } catch (error) {
      return this.res.serverError({
        message: this.req.i18n.__("database_error"),
        data: { error },
      });
    }
  },
};

const Appointment = require("../schema/Appointment");
const Service = require("../schema/Service");
const moment = require("moment");

module.exports = {
  friendlyName: "Can take appointment",

  description: "",

  inputs: {
    settings: {
      type: "ref",
      required: true,
    },
    user: {
      type: "string",
      required: true,
    },
  },

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs, exits) {
    const tomorrow = moment().add(1, "day");
    const lastDay = moment().add(4, "day");

    const startTime = moment(inputs.settings.startTime).set({
      year: tomorrow.year(),
      month: tomorrow.month(),
      date: tomorrow.date(),
    });

    const endTime = moment(inputs.settings.endTime).set({
      year: lastDay.year(),
      month: lastDay.month(),
      date: lastDay.date(),
    });

    // Get all active appointments for the user for the next 3 days.
    const reserved = await Appointment.find([
      ["userId", "==", inputs.user],
      ["status", "==", "active"],
      ["date", ">=", startTime],
      ["date", "<=", endTime],
    ]);

    if (reserved.length >= 3) {
      return exits.error(
        "You can't reserve new appointments for the next 3 days. You have already 3 active appointments"
      );
    }

    const services = await Service.find([]);
    const estimations = [];

    for (const service of services) {
      const times = [];

      let currentTime = tomorrow.clone();

      while (currentTime.toDate() <= endTime.toDate()) {
        const excludeTimes = reserved
          .filter((appointment) => {
            
            return (
              appointment.serviceId === service.id &&
              moment(appointment.date.toDate()).isBetween(
                currentTime.startOf("day").toDate(),
                currentTime.endOf("day").toDate(),
                "minute",
                "[]"
              )
            );
          })
          .map((a) => moment(a.date.toDate()).format("HH:mm"));

        const timeSlot = await sails.helpers.timeSloter.with({
          excludeTimes,
          startTime: moment(inputs.settings.startTime).format("HH:mm"),
          endTime: moment(inputs.settings.endTime).format("HH:mm"),
          nextSlot: inputs.settings.slotTime,
        });

        times.push({ [currentTime.format("YYYY-MM-DD")]: timeSlot.slots });

        currentTime = currentTime.add(1, "day");
      }

      estimations.push({ service, times });
    }

    return exits.success(estimations);
  },
};

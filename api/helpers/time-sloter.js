const moment = require("moment");

const isInBreak = function (pair, excludeTimes) {
  return excludeTimes.some((ex) => {
    return moment(ex, "HH:mm").isBetween(
      moment(pair[0], "HH:mm").toDate(),
      moment(pair[1], "HH:mm").toDate(),
      "minute",
      "[]"
    );
  });
};

module.exports = {
  friendlyName: "Time sloter",

  description: "Generate time slot for a given date.",

  inputs: {
    nextSlot: {
      type: "number",
      description: "Solt duration in minutes",
      defaultsTo: 30,
    },
    gap: {
      type: "number",
      description: "Gap duration between slots",
      defaultsTo: 0,
    },
    time: {
      type: "string",
      description: `Target time (e.g."11:00")`,
    },
    excludeTimes: {
      type: "ref",
      description: `Array of break hours (e.g. ["11:00", "14:00"])`,
      defaultsTo: [],
    },
    startTime: {
      type: "string",
      description: "Start time. (format: HH:mm)",
      required: true,
    },
    endTime: {
      type: "string",
      description: "End time. (format: HH:mm)",
      required: true,
    },
  },

  exits: {
    success: {
      description: "Time slots generated successfully.",
    },
  },

  fn: async function (inputs) {
    var slotTime = moment(inputs.startTime, "HH:mm");
    var endTime = moment(inputs.endTime, "HH:mm");

    let slots = [];
    let selected = [];
    let index = 0;
    while (slotTime.toDate() < endTime.toDate()) {
      const nextTime = slotTime.clone().add(inputs.nextSlot, "minutes");
      const pair = [slotTime.format("HH:mm"), nextTime.format("HH:mm")];

      selected = inputs.time && isInBreak(pair, [inputs.time]) ? pair : selected;

      if (!isInBreak(pair, inputs.excludeTimes)) {
        slots.push(pair);
      }

      if (!(index % 2)) {
        nextTime.add(inputs.gap, "minutes");
      }

      slotTime = nextTime;

      index++;
    }

    return { selected, slots };
  },
};

module.exports = {


  friendlyName: 'Seed',


  description: 'Seed mockup data.',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    require('../seeders');
  }

};


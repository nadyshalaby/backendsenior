const Appointment = require("../schema/Appointment");
const Service = require("../schema/Service");
const moment = require("moment");

module.exports = {
  friendlyName: "Can take appointment",

  description: "",

  inputs: {
    settings: {
      type: "ref",
      required: true,
    },
    slot: {
      type: "ref",
      required: true,
    },
    time: {
      type: "ref",
      required: true,
    },
    user: {
      type: "string",
      required: true,
    },
    service: {
      type: "string",
      required: true,
    },
  },

  exits: {
    success: {
      description: "All done.",
    },
  },

  fn: async function (inputs, exits) {
    const due_date = moment(inputs.time);

    // Check if appointment is in the future
    if (due_date.isBefore(moment())) {
      return exits.error("Appointment is in the past");
    }

    // Check if appointment is during the working hours
    if (
      !due_date.isBetween(
        moment(inputs.settings.startTime).set({
          year: moment().year(),
          month: moment().month(),
          date: moment().date(),
        }),
        moment(inputs.settings.endTime).set({
          year: due_date.year(),
          month: due_date.month(),
          date: due_date.date(),
        }),
        "minute",
        "[]"
      )
    ) {
      return exits.error("Appointment is outside working hours");
    }

    const service = await Service.findById(inputs.service);

    // Check if service is available
    if (!service) {
      return exits.error("Service not found");
    }

    // Check if service is enabled
    if (!service.enabled) {
      return exits.error("Service is disabled");
    }

    // Get all active appointments for the user.
    const reserved = await Appointment.find([
      ["userId", "==", inputs.user],
      ["status", "==", "active"],
    ]);

    if (reserved.length >= 3) {
      return exits.error("You have already 3 active appointments");
    }

    // Check if service is already reserved and still active
    const isServiceReserved = reserved.find(
      (appointment) =>
        appointment.serviceId === inputs.service &&
        appointment.status === "active"
    );

    if (isServiceReserved) {
      return exits.error("Service is already reserved");
    }

    // Check if the requested service could handle a new appointment with the given time slot.
    const slotStart = moment(inputs.slot[0], "HH:mm");
    const slotEnd = moment(inputs.slot[1], "HH:mm");
    const activeAppointmentsWithTimeSolt = await Appointment.find([
      ["status", "==", "active"],
      ["serviceId", "==", inputs.service],
      ["date", ">=", slotStart.toDate()],
      ["date", "<=", slotEnd.toDate()],
    ]);

    console.log(activeAppointmentsWithTimeSolt)

    if (activeAppointmentsWithTimeSolt.length >= 30) {
      return exits.error("Please choose another time, Service reservation is full at this time.");
    }

    return exits.success(true);
  },
};

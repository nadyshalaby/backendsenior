/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {
  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  // models: {
  //   connection: 'someMongodbServer'
  // }
  jwtSecret: 'secret|Jwt|prefered|token@#2@',
  jwtExpires: 60 * 60 * 2,

  //TODO: add your firebase Access_KEYS
  firebase_config: {
    url: 'https://suplift-b5a1c-default-rtdb.firebaseio.com/',
    type: 'service_account',
    project_id: 'suplift-b5a1c',
    private_key_id: '3cace4f769b8850e9ef266c3447f6d294343ccb1',
    private_key:
      '-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDYcZ/gJEZKXqBX\nt+LcWt3IJcGBeGd2mtRV/LUfGUwTL535dtGe/4uxNfCi9EyPsyvk1+JPA67/lmeL\n97p++msdIfMh1AZD+hXXde+BcbvZ7Ad0gNGWNbhkzs9IPfLLbDOPwTNS70BLGeEv\n5145HKXP6VKvzBC4ldmVkektHJCCI5E34GppbyaB8dSHuRF1TwRUtffibykvhkxG\nOFrJAAWmZl36BsWoHca3aatpgGVibdIUTFK55YSWha7sJu/PrOT9zgGhKZ+LSyTA\neLjU6f11d6xb6o9tB3dNnaM3ekhvh6inkxV/qYJF5ZSRGEeijMe73kd1ejJuMUv8\nsZeK159JAgMBAAECggEACwIYZ1jLnxEEpwF0udAAnfiBniPxYIBhEpsphmfjInSr\nsa+KhhjUJ31T/E21g8zRNjCJU4sfQDjg56b//AugyANdL21uViW8LPr+upQowYgg\nxvBITeifNLf/UIV0uZuKscyEijrIh9P3S9hAxrcqHb1tqwA+IiKI8Ej9VYHcPaoZ\nF4kmMabnNdFLcVX7jICW9Iu5GEikZFllNy4oY092ZwbW6U2zBK7w95qPKiA7wSAj\nKSjzFaQwzEuHy25OnYk1LkWlbYREPq8JkPLG0YazTS9NQ/+8RhBhpwOgtjEtw3uW\nP9zUprGXnmM78bg0Z0PR7tOIbDFihOl6sP5g5KPOqwKBgQD8Z3GQI/itbJCkR3wH\nw4oOaRp6gztcCsnX8fZ/Rr5ynz3Q4Yk8GzIywIX+7xuQ1WmPwpicvitIhRzTLxES\nwFJ91JWr55x49V1UWq5p0/m+mnmabhmmOuQ8+3Rje0BrqSVrgVcT0uGaw31LnJ9h\n6AafkGWvEQgbMJ8S1u6rA5if8wKBgQDbhwdHHh50HNrWrMFUGw6KvjPe3Gz0m8kf\nQBFMpLBFTaVPmmvSdYlcIyor/yTzk9+QMC5zQmd+9xrLDK9NHbQUlexv9B/+DMLf\n0tAF1mnNA+8Lv662V21ABnZUcp4yfQjl7f66lAtNIp5hbu/1oRxEgxGH8GJePFRj\nYP9WvyGO0wKBgQDRG4Y7C4Ifc10VrN91sysyKyMzsntAEvQMArrb0SlA3uqgnelL\nMaDR6SxZ+5BT2mq9HlO/L9G+IAG0eD7l0Q3Gj5N8OsModubeVs4+gM5cebAbH/IC\nBLJIT26qaFcDmruUqDATDVe5FxI7zUNCIq8yDLghZLPZituPCR/r8GvGvwKBgFSi\n25B0sUMM+OY5VpQJlnKxYCg1DaDFCyaMOd81w2wEwo5NN/K9NAcc25540RND2Lar\njM0NNf/CX0cXjeKKaCpjnvOOyGLXIq7OhTsq7kGHmbsK3N4aGNnNZAOs6QcII80a\nE9J1T2Iezbc84TDvO4nCipjBefthARwLgoY8TEDXAoGAJxnOGd7cH+RiEOEfpnSf\ndpc6t9kTKNTLGxqWfKBb7/riAmvxOHr8o19AzC93xX3LQJDAK0QbHRZMWvsXOyOp\n21pvu9SMnzwD5ifI+aLbV3aHqsSj0xiYU/aVPBPdw/CFokFDMcaH/53YjVFnQeC0\nJVM1uD9ETNVXTUecThC1IMc=\n-----END PRIVATE KEY-----\n',
    client_email:
      'firebase-adminsdk-ci5yq@suplift-b5a1c.iam.gserviceaccount.com',
    client_id: '117506995759772615301',
    auth_uri: 'https://accounts.google.com/o/oauth2/auth',
    token_uri: 'https://oauth2.googleapis.com/token',
    auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
    client_x509_cert_url:
      'https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-ci5yq%40suplift-b5a1c.iam.gserviceaccount.com',
  },
  algolia_config: {
    //TODO: add your algolia Access_KEYS
    admin_api_key: 'c95848ac035022a24e2adee338a7ae63',
    api_id: 'O479JS784V',
    index_name: 'dev_suplift',
  },
};
